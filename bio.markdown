---
layout: default
title: Biography
permalink: /bio
---

Lewis Davidson (born 1990) is a Mixed Media Artist, based in London. His work addresses themes of value, transience and displacement through Sculpture, Photography and Installation.

In 2010 Lewis undertook a BA in Fine Art at Chelsea School of Art and Design. 2014-16 he was part of the touring Art@Tell Programme, directed by the University of St. Gallen, Switzerland. 2017-19 Lewis studied at Slade School of Fine Art (MFA, Sculpture), with the addition of the Felix Slade Scholarship and the Deans Bursary. Upon graduation he was awarded the Almacantar Studio Award (2019) and placed on the Deans List of Academic Excellence at UCL.

Exhibitions include GARGLE (Bow Arts, London, 2020), Season of Fire from the Passenger Seat (Raw Labs, London, 2020), Adjacent Directly (Unit 1, London, 2019), Under Sole and Sky Head (Slade School of Fine Art, London, 2019), Holiday (ARCADE, London, 2018), Catch of the Year (DIENSTGEBÄUDE, Zürich, 2017) and The Observer Effect (GEMAK, The Hague, 2015). Residencies include Duo Residency (Unit 1, London, 2020).
Upcoming exhibitions include Solo Show Gearhead (A Room Upstairs, London, 2021).
