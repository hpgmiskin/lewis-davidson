from typing import Tuple, List

import re
import json
import time
import logging
import urllib.parse
import os.path

import requests
from bs4 import BeautifulSoup

WEBSITE = "http://lewisdavidsonartist.co.uk"
SLEEP_DURATION = 10

logger = logging.getLogger(__name__)


def load_json(filename: str):
    with open(filename) as open_file:
        return json.load(open_file)


def get_cache_filename(path: str):
    return ".cache" + path + ".html"


def load_cache(path: str):
    filename = get_cache_filename(path)
    with open(filename) as open_file:
        return open_file.read()


def write_cache(path: str, content: str):
    filename = get_cache_filename(path)
    with open(filename, "w") as open_file:
        open_file.write(content)


def download_page(page_path: str):
    try:
        content = load_cache(page_path)
        logger.debug("using cached version")
    except FileNotFoundError:
        page_address = WEBSITE + page_path
        logger.debug("downloading non cached version")
        content = requests.get(page_address).text
        write_cache(page_path, content)
        logger.info("sleep for %s seconds", SLEEP_DURATION)
        time.sleep(SLEEP_DURATION)

    return content


class PageData:
    title: str
    description: str
    image_url: str


TITLE_MAP = {
    "Puzzles (Close-up)": "Puzzles",
    "Bowling Ball (Close-up)": "Bowling Balls",
    "Tag": "Tags",
}


def lookup_title(title: str) -> str:
    return TITLE_MAP.get(title, title)


def process_page(content: str):
    soup = BeautifulSoup(content, "html.parser")
    main = soup.find("div", id="main")
    page_data = PageData()
    page_data.title = lookup_title(main.find("h2").text)
    page_data.description = main.find("p").text
    page_data.image_url = main.find("img").get("src")
    return page_data


def load_shows():
    shows = {}
    for page in load_json("./data/pages.json"):
        page_without_slash = page[1:]
        content = download_page(page)
        page_data = process_page(content)
        if page_data.title in shows:
            show = shows[page_data.title]
            show["pages"].append(page_without_slash)
            show["descriptions"].append(page_data.description)
            show["image_urls"].append(page_data.image_url)
        else:
            shows[page_data.title] = {
                "title": page_data.title,
                "pages": [page_without_slash],
                "descriptions": [page_data.description],
                "image_urls": [page_data.image_url],
            }
    return shows.values()


def download_show_image(images_folder: str, image_url: str) -> str:
    parsed_image_url = urllib.parse.urlparse(image_url)
    image_name = os.path.basename(parsed_image_url.path)
    local_image_path = os.path.join(images_folder, image_name)
    if os.path.exists(local_image_path):
        logger.info("image exists %s", local_image_path)
        return local_image_path
    # Create the local folder
    local_folder = os.path.split(local_image_path)[0]
    os.makedirs(local_folder, exist_ok=True)
    # Download the image to the local file
    logger.info("downloading %s", local_image_path)
    with open(local_image_path, "wb") as open_file:
        response = requests.get(image_url, stream=True)
        for chunk in response.iter_content(chunk_size=128):
            open_file.write(chunk)
    # When download complete sleep to not get blocked
    logger.info("sleep for %s seconds", SLEEP_DURATION)
    time.sleep(SLEEP_DURATION)
    return local_image_path


def download_show_images(name: str, show: dict) -> List[str]:
    local_image_paths = []
    local_folder = "./images/{}".format(name)
    for image_url in show["image_urls"]:
        local_image_path = download_show_image(local_folder, image_url)
        local_image_paths.append(local_image_path)
    return local_image_paths


def convert_image_paths(local_paths: List[str], prefix: str = "/") -> List[str]:
    return [local_path.replace("./", prefix) for local_path in local_paths]


post_template = """---
layout: post
title: {title}
cover_image: {cover_image}
images_folder: {images_folder}
---

{description}
"""


def save_post(filename, post_data):

    post_content = post_template.format(**post_data)

    filepath = "_posts/" + filename
    with open(filepath, "w") as open_file:
        open_file.write(post_content)


pattern = re.compile(r"/wp-content/uploads/(?P<year>\d+)/(?P<month>\d+)")


def parse_image_date(image_url: str) -> Tuple[str, str]:
    match = pattern.search(image_url)
    year = match.group("year")
    month = match.group("month")
    return (year, month)


def create_name(year, month, title):
    title_name = re.sub(r"\W+", "-", title).lower()
    return "{}-{}-01-{}".format(year, month, title_name)


def export_posts(shows):
    posts = []
    for show in shows:
        logger.info("processing %s", show["title"])
        year, month = parse_image_date(show["image_urls"][0])
        name = create_name(year, month, show["title"])
        descriptons = [d.replace("\n", " - ") for d in show["descriptions"]]
        local_image_paths = download_show_images(name, show)
        image_urls = convert_image_paths(local_image_paths)
        images = zip(descriptons, image_urls)
        save_post(
            "{}.markdown".format(name),
            {
                "name": name,
                "title": show["title"],
                "description": descriptons[0],
                "cover_image": image_urls[0],
                "images_folder": os.path.commonpath(image_urls),
            },
        )


def main():
    shows = load_shows()
    export_posts(shows)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()