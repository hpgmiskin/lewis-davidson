function toggleLinks() {
    const links = document.querySelector("ul.links");
    links.classList.toggle("mobile-hidden");
}

document.querySelector("#navbar-toggle").addEventListener("click", toggleLinks);
