---
layout: post
position: 2
title: Under Sole and Sky Head
material: Clear Vacuum Formed Plastic Moulds, 400 x 600cm Billboard Print, C Type Print on Stickers, Sporadic Bleeps emitting from hidden Speakers
size: Dimensions Variable
year: 2019
cover_image: /images/2019-07-01-under-sole-and-sky-head/01.jpg
images_folder: /images/2019-07-01-under-sole-and-sky-head
shows:
 - venue: A place with a name
   images_folder: /images/2019-07-01-under-sole-and-sky-head
---

In the multi-media installation Under Sole and Sky Head, Davidson articulates and explores the temporary  and throw-away nature of Consumerist Culture through the materials, approaches and icons it uses preserve, advertise and monitor value.

Behind the walls emit harsh, sporadic bleeping sounds. These are reconfigured samples recorded from train ticket barriers, supermarket checkouts, cash machines and security doors. Out of place they loose their purpose as markers of transaction, yet still carry a sense of urgency and alarm.

Flying in formation at varying heights, large clear vacuum form packaging moulds divide and distort the space. Suspended from floor to ceiling, the moulds capture the absent forms of Frankenstein-like products, composed of broken and unwanted objects. Working as twisted windows, they block and frame the billboard sized blue sky backdrop, Bogus.

Bogus is a photograph taken of a 30 x 20cm image found on an advertisement for housing developments in Davidson’s local area. Small sections of blue sky were bracketed between the proposed flats. Davidson was drawn to expanding the scale of the blue sky and removing the buildings. Within Under Sole and Sky Head, Bogus provides a flat illusion of space and an intangible context for absent products, fictional logos and displaced sounds. 