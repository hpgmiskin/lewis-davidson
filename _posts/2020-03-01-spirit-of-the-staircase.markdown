---
layout: post
position: 3
title: Spirit of the Staircase
material: Clear Vacuum Formed Plastic Moulds made from assorted Objects, Stainless Steel fixings
size: 55 x 75 x 25cm - 195 x 75 x 25cm
year: 2019 - 2020
cover_image: /images/2020-03-01-spirit-of-the-staircase/2020-inner-space/01.jpg
images_folder: /images/2020-03-01-spirit-of-the-staircase
shows:
 - venue: Inner Space, Oliver Beer Studios, Westminster, London, 2020
   images_folder: /images/2020-03-01-spirit-of-the-staircase/2020-inner-space 
 - venue: Adjacent Directly, Unit1 Gallery, Shepherds Bush, London, 2019 
   images_folder: /images/2020-03-01-spirit-of-the-staircase/2019-adjacent-directly 
---

The series, Spirit of the Staircase, began when Lewis noticed a discarded blister packet for toy binoculars. Although the binoculars were now absent, through the form of the clear plastic, Davidson could see the product the packaging had once held. This realisation opened a window to a new means of making. 

Working solely with broken and discarded products, Davidson assembles and re-configures disparate parts to create Frankenstein-like products. 

Putting the hybrid products directly through a large Vacuum Former, the momentary assemblages are captured. The thin skin of the plastic mould cohesively joins jarring materials and objects of distant contexts and purposes that were once fantastical and ridiculous in the flesh. Upon removing them from the Former they fall apart, their impression on the plastic mould is all that is left. Only in their absence do these fictional forms become believable.

Davidson describes the process as a from of physical photography. The Vacuum Former captures the momentary and in turn the clear plastic mould acts as a lens; distorting and framing the space around it.