---
layout: default
title: Curriculum Vitae
permalink: /cv
---

## Education
* 2017-19 MFA Sculpture, Slade School of Fine Art (Distinction)
* 2010-13 BA Hons Fine Art, Chelsea School of Art and Design (1st Class Honours)
* 2009-10 Foundation Diploma in Art and Design, University of Gloucestershire (Distinction)
 
## Scholarships and Awards
* 2019 Almacantar Studio Award
* 2019 Deans List of Academic Excellence, Slade School of Fine Art
* 2018 Deans Bursary, Slade School of Fine Art
* 2018 Red Mansion Art Prize Shortlist
* 2017 Felix Slade Scholarship, Slade School of Fine Art
* 2014-16 Art@Tell, University of St.Gallen, Switzerland
* 2013 Flat Time House Award Shortlist
* 2010 Travel Scholarship, University of Gloucestershire
 
## Upcoming Solo Show
* 2021 Gearhead, A Room Upstairs, Tower Hamlets, London

## Selected Exhibitions
* 2020 GARGLE, a culmination of the Almacantar Studio Award, Raw Labs, Newham, London
* 2020 Inner Space, Oliver Beer Studios, Westminster, London
* 2020 Season of Fire from the Passenger Seat, Raw Labs, Newham, London
* 2019 Adjacent Directly, Unit1 Gallery, Shepherds Bush, London
* 2019 Under Sole and Sky Head, Slade School of Fine Art, Camden, London
* 2019 Perpetual Shift, The Koppel Project, Westminster, London
* 2019 I Still Don’t Know What Drives Us, The Zetter, City of London, London
* 2019 Limbo, Slade Summer School Residency, Camden, London
* 2019 Interim, Slade School of Fine Art, Camden, London
* 2018 Holiday, ARCADE, Islington, London, (self-initiated and curated)
* 2018 Small Press Project, Slade School of Fine Art, Camden, London
* 2017 Catch of the Year, DIENSTGEBÄUDE, Zürich, Switzerland
* 2017 CRUMBLE TOWN, SET Space, Southwark, London, (self-initiated and curated)
* 2016 Fat Relic, Lewisham, London
* 2016 Tag, The Zetter, City of London, London
* 2016 ART ON CAMPUS, Vienna University of Economics and Business, Vienna, Austria
* 2015 The Observer Effect, GEMAK, The Hague, Netherlands
* 2015 Fresh Air, Cirencester, Gloucestershire
* 2015 Little Mountains, Embassy Tea Gallery, Southwark, London, (co-curated)
* 2014 The Open West, The Wilson, Cheltenham, Gloucestershire
* 2014 Trophy for a Cabbie, The Zetter, City of London, London
* 2014 London/Vienna Calling, Semperdepot, Vienna, Austria, (co-curated)
* 2013 London/Vienna Calling, Mile End, London, (co-curated)
* 2013 Fresh Air, Cirencester, Gloucestershire
* 2012 A Momentary Holding, Gallery on the Corner, Lambeth, London (co-curated)
* 2012 And you, what do you Believe in?, The Crypt, Camden, London
* 2012 BRITTVINTER,Umea,Sweden
 
## Residencies
* 2020 Duo, Unit1 Gallery, Shepherds Bush, London
* 2019 Slade Summer School Residency, Camden, London
* 2016 Fat Relic, Lewisham, London
* 2016 Artist in Residence, Cody Dock Art School, Newham, London
* 2014-Ongoing Workshop 53, Aberfeldy Street, Tower Hamlets, London.
  _Self-initiated and maintained Wood Workshop, open to makers and designers seeking machines, assistance and technical advice._
 
## Commissioned Projects
* 2020, 2018 and 2016 James Dick
* 2019 and 2020 Marion and Christof Tresler
* 2014 Frontier Economics
* 2013 Carolle Farron
* 2012 Ruth MacIntosh
* 2012 Frontier Economics
* 2011 Glenn and Jane Tutsell
 
## Upcoming Talk
* 2020 Artist Talk, Conscious Isolation, Online
 
## Workshops and Talks
* 2020 Artist Talk, Conscious Isolation Fine Art Lecture Series
* 2020 Artist Talk, Norwich University of the Arts
* 2019 Out of Hand and into the Fire, Clay and Glass Workshop, Newcastle University of Architecture
* 2019 Frankenstein’s Shop, Charrette Week, Newcastle University of Architecture
* 2019 In Front of the Noise, a brief history of Logos in Punk Music, Slade Summer School
* 2019 Artist Development Talk, Chelsea School of Art and Design
